package org.sisyphus.peacock.base.state;

public record DefaultState(String id, String name, boolean isFinal) implements State {

    @Override
    public String toString() {
        return "DefaultState{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", isFinal=" + isFinal + '}';
    }
}
