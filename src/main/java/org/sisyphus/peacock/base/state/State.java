package org.sisyphus.peacock.base.state;

public interface State {

    String id();

    String name();

    boolean isFinal();

}
