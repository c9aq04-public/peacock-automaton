package org.sisyphus.peacock.base.answer;

public class NullValueException extends RuntimeException {

    public NullValueException(Class<?> clazz, String field) {
        super(makeMessage(clazz, field));
    }

    private static String makeMessage(Class<?> clazz, String field) {
        return String.format("Field %s of class %s must not be null", field, clazz);
    }
}
