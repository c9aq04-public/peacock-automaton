package org.sisyphus.peacock.base.answer;

import java.util.Objects;

public class ExactValueMatchingAnswer extends AbstractAnswer implements Answer {

    public ExactValueMatchingAnswer(String id, String name) {
        super(id, name);
    }

    @Override
    public boolean matches(Answer other) {
        return Objects.equals(this, other);
    }


    @Override
    public String toString() {
        return "ExactValueMatchingAnswer{" +
                "value='" + id() + '\'' +
                '}';
    }
}
