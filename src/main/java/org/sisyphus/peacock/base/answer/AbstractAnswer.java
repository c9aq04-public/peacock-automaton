package org.sisyphus.peacock.base.answer;

import java.util.Objects;

public abstract class AbstractAnswer implements Answer {

    private final String id;
    private final String name;

    protected AbstractAnswer(String id, String name) {
        checkNotNull(id);
        this.id = id;
        this.name = name;
    }

    private void checkNotNull(Object value) {
        if (Objects.isNull(value)) {
            throw new NullValueException(Answer.class, "value");
        }
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        ExactValueMatchingAnswer that = (ExactValueMatchingAnswer) o;
        return Objects.equals(id(), that.id());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id());
    }

    @Override
    public String toString() {
        return "AbstractAnswer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
