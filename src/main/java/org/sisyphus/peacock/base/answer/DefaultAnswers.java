package org.sisyphus.peacock.base.answer;

import java.util.List;
import java.util.Objects;

public record DefaultAnswers(List<Answer> answers) implements Answers {

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        DefaultAnswers that = (DefaultAnswers) o;
        return Objects.equals(answers, that.answers);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(answers);
    }

    @Override
    public String toString() {
        return "DefaultAnswers{" +
                "answers=" + answers +
                '}';
    }
}
