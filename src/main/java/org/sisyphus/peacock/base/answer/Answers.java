package org.sisyphus.peacock.base.answer;

import org.sisyphus.peacock.base.utils.CollectionSizeMismatchException;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 */
public interface Answers {

    List<Answer> answers();

    default boolean matches(List<Answer> answers) {
        checkSizes(answers(), answers);
        return IntStream.range(0, answers().size()).boxed().allMatch(index -> answers().get(index).matches(answers.get(index)));
    }

    default boolean matches(Answers answers) {
        return matches(answers.answers());
    }

    private static void checkSizes(List<Answer> expected, List<Answer> actual) {
        int expectedSize = Objects.isNull(expected) ? -1 : expected.size();
        int actualSize = Objects.isNull(actual) ? -2 : actual.size();
        if (expectedSize != actualSize) {
            throw new CollectionSizeMismatchException(expectedSize, actualSize);
        }
    }

}
