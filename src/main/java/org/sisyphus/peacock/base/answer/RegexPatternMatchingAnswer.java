package org.sisyphus.peacock.base.answer;

public class RegexPatternMatchingAnswer extends AbstractAnswer implements Answer {

    public RegexPatternMatchingAnswer(String id, String name) {
        super(id, name);
    }

    @Override
    public boolean matches(Answer other) {
        return other.id().matches(id());
    }
}
