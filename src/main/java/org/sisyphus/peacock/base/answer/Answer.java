package org.sisyphus.peacock.base.answer;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 */
public interface Answer {

    String id();

    String name();

    boolean matches(Answer other);
}
