package org.sisyphus.peacock.base.collatz;


import lombok.extern.slf4j.Slf4j;
import org.sisyphus.peacock.base.action.Action;
import org.sisyphus.peacock.base.action.DefaultAction;
import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.answer.Answer;
import org.sisyphus.peacock.base.answer.DefaultAnswers;
import org.sisyphus.peacock.base.answer.ExactValueMatchingAnswer;
import org.sisyphus.peacock.base.automaton.DefaultAutomaton;
import org.sisyphus.peacock.base.context.DefaultObjectSession;
import org.sisyphus.peacock.base.context.ObjectSession;
import org.sisyphus.peacock.base.context.SimpleKeyValueActionContext;
import org.sisyphus.peacock.base.context.SimpleKeyValueQuestionContext;
import org.sisyphus.peacock.base.decision.Decisions;
import org.sisyphus.peacock.base.decision.DefaultDecisions;
import org.sisyphus.peacock.base.question.DefaultQuestion;
import org.sisyphus.peacock.base.question.DefaultQuestions;
import org.sisyphus.peacock.base.question.NamedQuestion;
import org.sisyphus.peacock.base.question.Question;
import org.sisyphus.peacock.base.scenario.DefaultScenario;
import org.sisyphus.peacock.base.scenario.DefaultScenarios;
import org.sisyphus.peacock.base.state.DefaultState;
import org.sisyphus.peacock.base.state.State;

import java.util.List;
import java.util.Set;

@Slf4j
public class Main {

    public static final String NUMBER = "number";

    public static final Answer ANSWER_IS_EVEN = new ExactValueMatchingAnswer("isEven", "Number is even");
    public static final Answer ANSWER_IS_ODD = new ExactValueMatchingAnswer("isOdd", "Number is odd");
    public static final Answer ANSWER_IS_ONE = new ExactValueMatchingAnswer("isOne", "Number is one");
    public static final DefaultAnswers ANSWERS_IS_ONE = new DefaultAnswers(List.of(ANSWER_IS_ODD, ANSWER_IS_ONE));
    public static final Answer ANSWER_IS_NOT_ONE = new ExactValueMatchingAnswer("isNotOne", "Number is not one");
    public static final DefaultAnswers ANSWERS_IS_EVEN = new DefaultAnswers(List.of(ANSWER_IS_EVEN, ANSWER_IS_NOT_ONE));
    public static final DefaultAnswers ANSWERS_IS_ODD = new DefaultAnswers(List.of(ANSWER_IS_ODD, ANSWER_IS_NOT_ONE));

    public static final State STATE_IS_ONE = new DefaultState("isOne", "Number is one", true);
    public static final DefaultScenario SCENARIO_ONE = new DefaultScenario(ANSWERS_IS_ONE, STATE_IS_ONE);
    public static final State STATE_IS_ODD = new DefaultState("isOdd", "Number is odd but not one", false);
    public static final DefaultScenario SCENARIO_ODD = new DefaultScenario(ANSWERS_IS_ODD, STATE_IS_ODD);
    public static final State STATE_IS_EVEN = new DefaultState("isEven", "Number is even", false);
    public static final DefaultScenario SCENARIO_EVEN = new DefaultScenario(ANSWERS_IS_EVEN, STATE_IS_EVEN);
    public static final DefaultScenarios SCENARIOS = new DefaultScenarios(Set.of(SCENARIO_ONE, SCENARIO_ODD, SCENARIO_EVEN));

    public static final NamedAction<ObjectSession> ACTION_NOOP = Main.noop();
    public static final NamedAction<ObjectSession> ACTION_MULTIPLY_BY_3_AND_ADD_1 = Main.multiplyBy3AndAdd1();
    public static final NamedAction<ObjectSession> ACTION_DIVIDE_BY_2 = Main.divideBy2();
    public static final Decisions<ObjectSession> DECISIONS = makeObjectSessionDecisions();
    public static final String NO_ACTION = "No action";
    public static final String MULTIPLY_BY_3_AND_ADD_1 = "Multiply by 3 and add 1.";
    public static final String DIVIDE_BY_2 = "Divide by 2.";
    public static final String DOING = "Number is {}. Doing \"{}\"";

    public static void main(String[] args) {
        log.info("Beginning Collatz automaton...");
        ObjectSession session = new DefaultObjectSession();
        int value = 27;
        session.put(NUMBER, value);
        SimpleKeyValueActionContext<ObjectSession> actionContext = Main.makeObjectSessionSimpleKeyValueActionContext(session);
        SimpleKeyValueQuestionContext<ObjectSession> questionContext = Main.makeObjectSessionSimpleKeyValueQuestionContext(session);
        DefaultAutomaton<ObjectSession, ObjectSession> automaton = Main.makeAutomaton(questionContext, actionContext);
        log.trace("automaton is {}", automaton);
        automaton.execute();
        log.info("Done with Collatz automaton.");
    }

    private static DefaultAutomaton<ObjectSession, ObjectSession> makeAutomaton(SimpleKeyValueQuestionContext<ObjectSession> questionContext, SimpleKeyValueActionContext<ObjectSession> actionContext) {
        DefaultAutomaton<ObjectSession, ObjectSession> automaton = new DefaultAutomaton<>();
        automaton.setQuestions(makeQuestions());
        automaton.setQuestionContext(questionContext);
        automaton.setActionContext(actionContext);
        automaton.setDecisions(DECISIONS);
        automaton.setScenarios(SCENARIOS);
        return automaton;
    }

    private static Decisions<ObjectSession> makeObjectSessionDecisions() {
        Decisions<ObjectSession> decisions = new DefaultDecisions<>();
        decisions.put(STATE_IS_ONE, ACTION_NOOP);
        decisions.put(STATE_IS_ODD, ACTION_MULTIPLY_BY_3_AND_ADD_1);
        decisions.put(STATE_IS_EVEN, ACTION_DIVIDE_BY_2);
        return decisions;
    }

    private static DefaultQuestions<ObjectSession> makeQuestions() {
        return new DefaultQuestions<>(List.of(isEven(), isOne()));
    }

    private static SimpleKeyValueQuestionContext<ObjectSession> makeObjectSessionSimpleKeyValueQuestionContext(ObjectSession session) {
        SimpleKeyValueQuestionContext<ObjectSession> questionContext = new SimpleKeyValueQuestionContext<>();
        questionContext.putQuestion(isEven(), session);
        questionContext.putQuestion(isOne(), session);
        return questionContext;
    }

    private static SimpleKeyValueActionContext<ObjectSession> makeObjectSessionSimpleKeyValueActionContext(ObjectSession session) {
        SimpleKeyValueActionContext<ObjectSession> actionContext = new SimpleKeyValueActionContext<>();
        actionContext.putAction(ACTION_DIVIDE_BY_2, session);
        actionContext.putAction(ACTION_MULTIPLY_BY_3_AND_ADD_1, session);
        actionContext.putAction(ACTION_NOOP, session);
        return actionContext;
    }

    private static NamedQuestion<ObjectSession> isEven() {
        Question<ObjectSession> isEven = s -> (((Integer) s.get(NUMBER)) % 2) == 0 ? ANSWER_IS_EVEN : ANSWER_IS_ODD;
        return new DefaultQuestion<>("isEven", "Number is even", isEven);
    }

    private static NamedQuestion<ObjectSession> isOne() {
        Question<ObjectSession> isOne = s -> ((Integer) s.get(NUMBER)) == 1 ? ANSWER_IS_ONE : ANSWER_IS_NOT_ONE;
        return new DefaultQuestion<>("isOne", "Number is equal to one", isOne);
    }

    private static DefaultAction<ObjectSession> noop() {
        Action<ObjectSession> noop = s -> {
            Object number = s.get(NUMBER);
            logAction(number, NO_ACTION);
            return s;
        };
        return new DefaultAction<>("noop", NO_ACTION, noop);
    }

    private static DefaultAction<ObjectSession> multiplyBy3AndAdd1() {
        Action<ObjectSession> multiplyBy3AndAdd1 = s -> {
            Object number = s.get(NUMBER);
            logAction(number, MULTIPLY_BY_3_AND_ADD_1);
            int multipliedBy3AndAdded1 = 1 + ((Integer) number) * 3;
            s.put(NUMBER, multipliedBy3AndAdded1);
            return s;
        };
        return new DefaultAction<>("multiplyBy3AndAdd1", MULTIPLY_BY_3_AND_ADD_1, multiplyBy3AndAdd1);
    }

    private static void logAction(Object number, String action) {
        log.debug(DOING, number, action);
    }

    private static DefaultAction<ObjectSession> divideBy2() {
        Action<ObjectSession> divideBy2 = s -> {
            Object number = s.get(NUMBER);
            logAction(number, DIVIDE_BY_2);
            int dividedBy2 = ((Integer) s.get(NUMBER)) / 2;
            s.put(NUMBER, dividedBy2);
            return s;
        };
        return new DefaultAction<>("divideBy2", DIVIDE_BY_2, divideBy2);
    }


}