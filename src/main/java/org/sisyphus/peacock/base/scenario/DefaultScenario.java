package org.sisyphus.peacock.base.scenario;

import org.sisyphus.peacock.base.answer.Answers;
import org.sisyphus.peacock.base.state.State;

import java.util.Objects;

/**
 * A scenario is something like:<br>
 * <ul>
 *     <li>A bunch of answers to questions.</li>
 *     <li>A state that represents those answers.</li>
 * </ul>
 * @param answers
 * @param state
 */
public record DefaultScenario(Answers answers, State state) implements Scenario {
    @Override
    public boolean matches(Answers answers) {
        return answers().matches(answers);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        DefaultScenario that = (DefaultScenario) o;
        return Objects.equals(state, that.state) && Objects.equals(answers, that.answers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answers, state);
    }

    @Override
    public String toString() {
        return "DefaultScenario{" +
                "answers=" + answers +
                ", state=" + state +
                '}';
    }
}
