package org.sisyphus.peacock.base.scenario;

import java.util.Collection;

public record DefaultScenarios(Collection<Scenario> scenarios) implements Scenarios {
    @Override
    public String toString() {
        return "DefaultScenarios{" +
                "scenarios=" + scenarios +
                '}';
    }
}
