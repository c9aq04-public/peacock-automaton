package org.sisyphus.peacock.base.scenario;

import org.sisyphus.peacock.base.answer.Answers;

import java.util.Collection;
import java.util.function.Supplier;

public interface Scenarios {

    Collection<Scenario> scenarios();

    default Scenario retrieve(Answers answers) throws NoSuchScenarioException {
        return scenarios().stream().filter(scenario -> scenario.matches(answers)).findFirst().orElseThrow(noSuchScenarioExceptionSupplier(answers));
    }

    private static Supplier<NoSuchScenarioException> noSuchScenarioExceptionSupplier(Answers answers) {
        return () -> new NoSuchScenarioException(answers);
    }

}
