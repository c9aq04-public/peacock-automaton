package org.sisyphus.peacock.base.scenario;

import org.sisyphus.peacock.base.answer.Answers;

public class NoSuchScenarioException extends RuntimeException {

    public NoSuchScenarioException(Answers answers) {
        super(String.format("No scenario corresponds to answers %s", answers.toString()));
    }
}
