package org.sisyphus.peacock.base.scenario;

import org.sisyphus.peacock.base.answer.Answers;
import org.sisyphus.peacock.base.state.State;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 */
public interface Scenario {

    Answers answers();

    boolean matches(Answers answers);

    State state();
}
