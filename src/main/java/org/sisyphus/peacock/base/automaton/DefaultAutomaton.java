package org.sisyphus.peacock.base.automaton;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.sisyphus.peacock.base.context.ActionContext;
import org.sisyphus.peacock.base.answer.Answer;
import org.sisyphus.peacock.base.answer.Answers;
import org.sisyphus.peacock.base.answer.DefaultAnswers;
import org.sisyphus.peacock.base.decision.Decisions;
import org.sisyphus.peacock.base.context.QuestionContext;
import org.sisyphus.peacock.base.question.Questions;
import org.sisyphus.peacock.base.scenario.Scenarios;

import java.util.List;

@Slf4j
@Setter
public class DefaultAutomaton<QUESTIONED, ACTED> implements Automaton<QUESTIONED, ACTED> {

    private QuestionContext<QUESTIONED> questionContext;
    private Questions<QUESTIONED> questions;
    private Scenarios scenarios;
    private ActionContext<ACTED> actionContext;
    private Decisions<ACTED> decisions;

    @Override
    public QuestionContext<QUESTIONED> questionContext() {
        return this.questionContext;
    }

    @Override
    public Questions<QUESTIONED> questions() {
        return this.questions;
    }

    @Override
    public Scenarios scenarios() {
        return this.scenarios;
    }

    @Override
    public ActionContext<ACTED> actionContext() {
        return this.actionContext;
    }

    @Override
    public Decisions<ACTED> decisions() {
        return this.decisions;
    }

    @Override
    public Answers answersFrom(List<Answer> answers) {
        return new DefaultAnswers(answers);
    }

    @Override
    public ACTED iterate() {
        log.debug("iterate()");
        var answers = answersFrom(questions().stream().map(this::ask).toList());
        log.trace("answers: {}", answers);
        var scenario = scenarios().retrieve(answers);
        log.trace("scenario: {}", scenario);
        var state = scenario.state();
        log.trace("state: {}", state);
        var action = decisions().retrieve(state);
        log.trace("action: {}", action);
        ACTED returned = act(action);
        return state.isFinal() ? null : returned;
    }


    @Override
    public String toString() {
        return "DefaultAutomaton{" +
                "questionContext=" + questionContext +
                ", questions=" + questions +
                ", scenarios=" + scenarios +
                ", actionContext=" + actionContext +
                ", decisions=" + decisions +
                '}';
    }
}
