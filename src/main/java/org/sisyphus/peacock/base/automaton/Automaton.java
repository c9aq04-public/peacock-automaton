package org.sisyphus.peacock.base.automaton;

import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.context.ActionContext;
import org.sisyphus.peacock.base.answer.Answer;
import org.sisyphus.peacock.base.answer.Answers;
import org.sisyphus.peacock.base.decision.Decisions;
import org.sisyphus.peacock.base.question.NamedQuestion;
import org.sisyphus.peacock.base.context.QuestionContext;
import org.sisyphus.peacock.base.question.Questions;
import org.sisyphus.peacock.base.scenario.Scenarios;

import java.util.List;
import java.util.Objects;

public interface Automaton<QUESTIONED, ACTED> {

    QuestionContext<QUESTIONED> questionContext();

    Questions<QUESTIONED> questions();

    Scenarios scenarios();

    ActionContext<ACTED> actionContext();

    Decisions<ACTED> decisions();

    Answers answersFrom(List<Answer> answers);

    ACTED iterate();

    default void execute() {
        var goOn = true;
        while (goOn) {
            goOn = Objects.nonNull(iterate());
        }
    }

    default ACTED act(NamedAction<ACTED> namedAction) {
        var actee = actionContext().forAction(namedAction);
        return namedAction.on(actee);
    }

    default Answer ask(NamedQuestion<QUESTIONED> namedQuestion) {
        var askee = questionContext().forQuestion(namedQuestion);
        return namedQuestion.ask(askee);
    }

}
