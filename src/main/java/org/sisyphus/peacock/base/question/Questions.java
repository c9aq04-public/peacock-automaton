package org.sisyphus.peacock.base.question;

import java.util.Collection;
import java.util.stream.Stream;

/**
 *
 * @param <QUESTIONED> supertype of all interviewed objects
 */
public interface Questions<QUESTIONED> {

    Collection<NamedQuestion<QUESTIONED>> questions();

    default Stream<NamedQuestion<QUESTIONED>> stream() {
        return questions().stream();
    }
}
