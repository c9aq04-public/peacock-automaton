package org.sisyphus.peacock.base.question;

import org.sisyphus.peacock.base.answer.Answer;

/**
 * @param <QUESTIONED> type of interrogated object
 */
public interface Question<QUESTIONED> {

    Answer ask(QUESTIONED askee);
}
