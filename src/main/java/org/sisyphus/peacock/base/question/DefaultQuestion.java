package org.sisyphus.peacock.base.question;

import lombok.extern.slf4j.Slf4j;
import org.sisyphus.peacock.base.answer.Answer;

@Slf4j
public class DefaultQuestion<QUESTIONED> implements NamedQuestion<QUESTIONED> {
    private final String id;

    private final String name;
    private final Question<QUESTIONED> question;
    public DefaultQuestion(String id, String name, Question<QUESTIONED> question) {
        this.id = id;
        this.name = name;
        this.question = question;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public Answer ask(QUESTIONED askee) {
        log.trace("Asking question {} to {}", this, askee);
        return question.ask(askee);
    }

    @Override
    public String toString() {
        return "DefaultQuestion{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", question=" + question +
                '}';
    }
}
