package org.sisyphus.peacock.base.question;

import java.util.Collection;
import java.util.List;

public class DefaultQuestions<QUESTIONED> implements Questions<QUESTIONED> {
    private final List<NamedQuestion<QUESTIONED>> questions;

    public DefaultQuestions(List<NamedQuestion<QUESTIONED>> questions) {
        this.questions = questions;
    }

    @Override
    public Collection<NamedQuestion<QUESTIONED>> questions() {
        return this.questions;
    }

    @Override
    public String toString() {
        return "DefaultQuestions{" +
                "questions=" + questions +
                '}';
    }
}
