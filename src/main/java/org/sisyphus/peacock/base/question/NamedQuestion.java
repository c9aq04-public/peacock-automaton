package org.sisyphus.peacock.base.question;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 * @param <QUESTIONED> type of interrogated object
 */
public interface NamedQuestion<QUESTIONED> extends Question<QUESTIONED> {

    String id();

    String name();

}
