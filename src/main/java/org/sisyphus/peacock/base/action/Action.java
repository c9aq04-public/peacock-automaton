package org.sisyphus.peacock.base.action;

/**
 * @param <ACTED> type of object acted upon
 */
@FunctionalInterface
public interface Action<ACTED> {

    ACTED on(ACTED actee);
}
