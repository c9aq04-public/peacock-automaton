package org.sisyphus.peacock.base.action;

import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

public abstract class AbstractNamedAction<ACTED> implements NamedAction<ACTED> {

    private final String id;

    private final String name;

    public AbstractNamedAction(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        AbstractNamedAction<?> that = (AbstractNamedAction<?>) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "AbstractAction{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }


}
