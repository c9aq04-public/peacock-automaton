package org.sisyphus.peacock.base.action;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultAction<ACTED> extends AbstractNamedAction<ACTED> implements NamedAction<ACTED> {

    private final Action<ACTED> action;

    public DefaultAction(String id, String name, Action<ACTED> action) {
        super(id, name);
        this.action = action;
    }

    @Override
    public ACTED on(ACTED actee) {
        log.trace("on({})", actee);
        return action.on(actee);
    }

    @Override
    public String toString() {
        return "DefaultAction{" +
                "action=" + action +
                '}';
    }
}
