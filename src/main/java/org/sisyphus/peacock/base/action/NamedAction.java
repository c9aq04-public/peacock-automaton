package org.sisyphus.peacock.base.action;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 *
 * @param <ACTED> type of object acted upon
 */
public interface NamedAction<ACTED> extends Action<ACTED> {

    String id();

    String name();

}
