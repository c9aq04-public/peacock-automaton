package org.sisyphus.peacock.base.decision;

import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.state.State;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 * @param <ACTED> type of objects acted on when answer is {@link Decision#state}
 */
public interface Decision<ACTED> {

    State state();

    NamedAction<ACTED> action();
}
