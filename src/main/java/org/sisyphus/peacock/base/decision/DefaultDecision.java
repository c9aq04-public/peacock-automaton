package org.sisyphus.peacock.base.decision;

import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.state.State;

public record DefaultDecision<ACTED>(State state, NamedAction<ACTED> action) implements Decision<ACTED> {

    @Override
    public String toString() {
        return "DefaultDecision{" +
                "state=" + state +
                ", action=" + action +
                '}';
    }
}
