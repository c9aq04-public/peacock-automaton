package org.sisyphus.peacock.base.decision;

import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.state.State;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DefaultDecisions<ACTED> implements Decisions<ACTED> {

    private final Map<State, Decision<ACTED>> map;

    public DefaultDecisions(Map<State, Decision<ACTED>> map) {
        this.map = map;
    }

    public DefaultDecisions() {
        this(new HashMap<>());
    }

    @Override
    public Collection<Decision<ACTED>> decisions() {
        return this.map.values();
    }

    @Override
    public NamedAction<ACTED> retrieve(State state) {
        return this.map.get(state).action();
    }

    @Override
    public void put(State state, NamedAction<ACTED> action) {
        this.map.put(state, new DefaultDecision<>(state, action));
    }

    @Override
    public String toString() {
        return "DefaultDecisions{" +
                "map=" + map +
                '}';
    }
}
