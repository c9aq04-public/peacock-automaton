package org.sisyphus.peacock.base.decision;

import org.sisyphus.peacock.base.state.State;
import org.sisyphus.peacock.base.action.NamedAction;

import java.util.Collection;

/**
 * Must implement {@link Object#hashCode}, {@link Object#equals} and {@link Object#toString}
 * @param <ACTED> a supertype of all objects affected by {@link Decisions#decisions}
 */
public interface Decisions<ACTED> {

    Collection<Decision<ACTED>> decisions();

    NamedAction<ACTED> retrieve(State state);

    void put(State state, NamedAction<ACTED> action);
}
