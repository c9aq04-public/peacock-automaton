package org.sisyphus.peacock.base.utils;

public class CollectionSizeMismatchException extends RuntimeException {
  private static final String NOT_INITIALIZED = " collection has not been initialized";

  public CollectionSizeMismatchException(int expected, int actual) {
        super(makeMessage(expected, actual));
    }

  private static String makeMessage(int expected, int actual) {
      if (expected < 0) {
        return "<expected>" + NOT_INITIALIZED;
      }
      if (actual < 0) {
        return "<actual>" + NOT_INITIALIZED;
      }
      return String.format("<expected> size is %s and <actual> size is %s", expected, actual);
  }
}
