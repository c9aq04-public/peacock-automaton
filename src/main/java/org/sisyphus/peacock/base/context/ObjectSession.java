package org.sisyphus.peacock.base.context;

public interface ObjectSession extends Session<Object> {
    /*
     * Nothing here
     */
}
