package org.sisyphus.peacock.base.context;

import java.util.HashMap;
import java.util.Map;

public class DefaultObjectSession implements ObjectSession {

    private final Map<String, Object> map = new HashMap<>();

    @Override
    public Object get(String name) {
        return this.map.get(name);
    }

    @Override
    public void put(String name, Object object) {
        this.map.put(name, object);
    }

    @Override
    public String toString() {
        return "DefaultObjectSession{" +
                "map=" + map +
                '}';
    }
}
