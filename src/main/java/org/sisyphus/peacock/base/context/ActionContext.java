package org.sisyphus.peacock.base.context;

import org.sisyphus.peacock.base.action.NamedAction;

public interface ActionContext<ACTED> {

    void putAction(NamedAction<ACTED> key, ACTED value);

    ACTED forAction(NamedAction<ACTED> object);

}
