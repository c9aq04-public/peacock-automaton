package org.sisyphus.peacock.base.context;

import org.sisyphus.peacock.base.question.NamedQuestion;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class SimpleKeyValueQuestionContext<RETURNED> implements QuestionContext<RETURNED> {

    private final Map<String, RETURNED> map;

    public SimpleKeyValueQuestionContext() {
        this(new HashMap<>());
    }

    public SimpleKeyValueQuestionContext(Map<String, RETURNED> map) {
        this.map = map;
    }

    public static <RETURNED> SimpleKeyValueQuestionContext<RETURNED> fromQuestions(Map<NamedQuestion<RETURNED>, RETURNED> map) {
        Map<String, RETURNED> returned = new HashMap<>();
        map.forEach(putKeyValue(returned));
        return new SimpleKeyValueQuestionContext<>(returned);
    }

    private static <RETURNED> BiConsumer<NamedQuestion<RETURNED>, RETURNED> putKeyValue(Map<String, RETURNED> returned) {
        return (k, v) -> returned.put(k.id(), v);
    }


    @Override
    public void putQuestion(NamedQuestion<RETURNED> key, RETURNED value) {
        this.map.put(key.id(), value);
    }

    @Override
    public RETURNED forQuestion(NamedQuestion<RETURNED> namedQuestion) {
        return this.map.get(namedQuestion.id());
    }

    @Override
    public String toString() {
        return "SimpleKeyValueQuestionContext{" +
                "map=" + map +
                '}';
    }
}
