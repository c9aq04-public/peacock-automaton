package org.sisyphus.peacock.base.context;

import org.sisyphus.peacock.base.action.NamedAction;
import org.sisyphus.peacock.base.question.NamedQuestion;

public class AllEncompassingContext<QUESTIONED, ACTED> implements QuestionContext<QUESTIONED>, ActionContext<ACTED> {

    private final QuestionContext<QUESTIONED> questionContext;
    private final ActionContext<ACTED> actionContext;

    public AllEncompassingContext(QuestionContext<QUESTIONED> questionContext, ActionContext<ACTED> actionContext) {
        this.questionContext = questionContext;
        this.actionContext = actionContext;
    }

    @Override
    public void putQuestion(NamedQuestion<QUESTIONED> key, QUESTIONED value) {
        questionContext.putQuestion(key, value);
    }

    @Override
    public QUESTIONED forQuestion(NamedQuestion<QUESTIONED> namedQuestion) {
        return questionContext.forQuestion(namedQuestion);
    }

    @Override
    public void putAction(NamedAction<ACTED> key, ACTED value) {
        actionContext.putAction(key, value);
    }

    @Override
    public ACTED forAction(NamedAction<ACTED> object) {
        return actionContext.forAction(object);
    }
}
