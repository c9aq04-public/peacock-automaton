package org.sisyphus.peacock.base.context;

import lombok.extern.slf4j.Slf4j;
import org.sisyphus.peacock.base.action.NamedAction;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class SimpleKeyValueActionContext<ACTED> implements ActionContext<ACTED> {
    private final Map<String, ACTED> map;

    public SimpleKeyValueActionContext() {
        this(new HashMap<>());
    }

    public SimpleKeyValueActionContext(Map<String, ACTED> map) {
        this.map = map;
    }

    @Override
    public void putAction(NamedAction<ACTED> key, ACTED value) {
        this.map.put(key.id(), value);
    }

    @Override
    public ACTED forAction(NamedAction<ACTED> object) {
        log.trace("Finding object to act on with {}", object);
        return map.get(object.id());
    }

    @Override
    public String toString() {
        return "SimpleKeyValueActionContext{" +
                "map=" + map +
                '}';
    }
}
