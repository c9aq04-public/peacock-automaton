package org.sisyphus.peacock.base.context;

import org.sisyphus.peacock.base.question.NamedQuestion;

public interface QuestionContext<QUESTIONED> {

    void putQuestion(NamedQuestion<QUESTIONED> key, QUESTIONED value);

    QUESTIONED forQuestion(NamedQuestion<QUESTIONED> namedQuestion);
}
