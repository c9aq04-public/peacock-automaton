package org.sisyphus.peacock.base.context;

public interface Session<T> {

    T get(String name);

    void put(String name, T object);

}
